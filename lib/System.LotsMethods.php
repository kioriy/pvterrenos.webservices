<?php
function registraLote($lotesActuales, $pk_predio, $pk_manzana, $lotesAgregar){
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}
	$lotesMas = (int)$lotesAgregar;
	$numero_lote = (int) $lotesActuales;
	if($numero_lote != 0){

		for($i = $numero_lote; $i <= $lotesMas; $i++){
			$query = "INSERT INTO Lote (n_lote, pk_predio, pk_manzana) VALUES ('$i', '$pk_predio', '$pk_manzana')";
			$result = $mysqli->query($query);
		}
			$mysqli->close();
			return new soapval('return','xsd:string', 'Lotes agregados con exito!');

	}
	else{
			for($i = 1 ; $i <= $lotesMas; $i++){
				$query = "INSERT INTO Lote (n_lote, pk_predio, pk_manzana) VALUES ('$i', '$pk_predio', '$pk_manzana')";
				$result = $mysqli->query($query);
			}
			$mysqli->close();
			return new soapval('return','xsd:string', 'Lotes ingresados con exito!');
		}

}

function cargaLotes($id_manzana){
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}
	$query = "SELECT `n_lote` FROM `Lote` WHERE `pk_manzana` ='$id_manzana' AND `status_lote` = 0 ORDER BY `Lote`.`n_lote` ASC" ;
	$datos = array();
	$result = $mysqli->query($query);
	while ( $data = $result->fetch_assoc()) {
		$datos[] = $data['n_lote'];
	};
	$lote = implode(",",$datos);
	$mysqli->close();
	return new soapval('return','xsd:string', $lote);

}

function cargaLotesVenta($id_manzana){
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}
	$query = "SELECT `n_lote` FROM `Lote` WHERE `pk_manzana` ='$id_manzana' AND `status_lote` = 1 ORDER BY `Lote`.`n_lote` ASC" ;
	$datos = array();
	$result = $mysqli->query($query);
	while ( $data = $result->fetch_assoc()) {
		$datos[] = $data['n_lote'];
	};
	$lote = implode(",",$datos);
	$mysqli->close();
	return new soapval('return','xsd:string', $lote);

}

function cargaLotesDePredio($id_predio){
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}
	$query = "SELECT `n_lote` FROM `Lote` WHERE `pk_predio` ='$id_predio' AND `status_lote` = 0 ORDER BY `Lote`.`n_lote` ASC" ;
	$datos = array();
	$result = $mysqli->query($query);
	while ( $data = $result->fetch_assoc()) {
		$datos[] = $data['n_lote'];
	};
	$lote = implode(",",$datos);
	$mysqli->close();
	return new soapval('return','xsd:string', $lote);

}

function updateLote($id_sesion, $id_lote, $pk_manzana, $estatus, $n_lote, $descripcion, $precio, $medida, $pk_norte, $pk_sur, $pk_este, $pk_oeste){
	$sesion = validaSesion($id_sesion);
	if($sesion != null){
		$id_usuario = $sesion->getId_user();
		$usuario = $sesion->getUsuario();
		if($usuario->getTipo() == 1){
			return new soapval('return','xsd:string', 'IN');
		}else{
			return new soapval('return','xsd:string', 'NO TIENE ACCESO');
		}
	}else{
		return new soapval('return','xsd:string','NO TIENE ACCESO');
	}
}

function getIdLote($id, $numero_lote,$nombreColumna){

	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
		if ($mysqli->connect_errno){
			printf("conexion fallida %s/n",$mysqli->connect_errno);
			exit();
		}
	$numero_lote = (int) $numero_lote;
	$query = "SELECT id_lote FROM Lote WHERE $nombreColumna = '$id' AND n_lote = '$numero_lote'" ;
	$result = $mysqli->query($query);
	$data = $result->fetch_assoc();
	$id_lote = $data['id_lote'];
	$mysqli->close();
	return new soapval('return','xsd:string', $id_lote);

}

function infoLote($idLote){
$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}
	$query = "SELECT `n_lote`, `pk_predio`, `pk_manzana` FROM `Lote` WHERE `id_lote` = '$idLote'";
	$result = $mysqli->query($query);
    $data = $result->fetch_assoc();

	$idManzana = $data['pk_manzana'];
	$idPredio = $data['pk_predio'];
	$numeroLote = $data['n_lote'];

    $queryManzana = "SELECT  `n_manzana` FROM `Manzana` WHERE `id_manzana` = ' $idManzana'";
	$resultM = $mysqli->query($queryManzana);
	$dataM = $resultM->fetch_assoc();
	$numeroManza = $dataM['n_manzana'];

	$queryPredio = "SELECT `nombre_predio` FROM `Predio` WHERE `id_predio` = '$idPredio'";
	$resultP = $mysqli->query($queryPredio);
	$dataP = $resultP->fetch_assoc();
	$nombrePredio = $dataP['nombre_predio'];

	//$datos = "$nombrePredio,$numeroManza";

    $mysqli->close();
	return new soapval('return','xsd:string',"$numeroLote,$nombrePredio,$numeroManza");

}

//Hugo favor de no borrar!!!!!!!!
function getInfoLotes($idLote){
$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}

	$query = "SELECT `n_lote`, `pk_predio`, `pk_manzana` FROM `Lote` WHERE `id_lote` = '$idLote'";
	$datos = array();
	$result = $mysqli->query($query);


    while ( $data = $result->fetch_assoc()) {

	$idManzana = $data['pk_manzana'];
	$idPredio = $data['pk_predio'];
	$numeroLote = $data['n_lote'];

    $queryManzana = "SELECT  `n_manzana` FROM `Manzana` WHERE `id_manzana` = ' $idManzana'";
	$resultM = $mysqli->query($queryManzana);
	$dataM = $resultM->fetch_assoc();
	$numeroManza = $dataM['n_manzana'];

	$queryPredio = "SELECT `nombre_predio` FROM `Predio` WHERE `id_predio` = '$idPredio'";
	$resultP = $mysqli->query($queryPredio);
	$dataP = $resultP->fetch_assoc();
	$nombrePredio = $dataP['nombre_predio'];

	$datos[] = $numeroLote.",".$nombrePredio.",".$numeroManza;
	};

	$DatosLote = implode("|",$datos);
    $mysqli->close();
	return new soapval('return','xsd:string',$DatosLote);

}

function getMedida(){
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
		if ($mysqli->connect_errno){
			printf("conexion fallida %s/n",$mysqli->connect_errno);
			exit();
		}

	$query = "SELECT `medida` FROM`MedidaLote` ";
	$datos = array();
						$result = $mysqli->query($query);
						while ( $data = $result->fetch_assoc()) {
						    $datos[] = $data['medida'];
						};
						$medidas = implode(",",$datos);
						$mysqli->close();
						return new soapval('return','xsd:string', $medidas);

}

function registraMedida($medida){
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}
	$query = "INSERT INTO MedidaLote (medida) VALUES ('$medida')";
	$result = $mysqli->query($query);
						$mysqli->close();
						return new soapval('return','xsd:string', 'Nueva medida Registrada con exito');
	}

function insertaMedida($id_lote, $medidaNorte, $medidaSur, $medidaEste, $medidaOeste){
$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}

$query = "UPDATE Lote SET medidaNorte= '$medidaNorte', medidaSur = '$medidaSur', medidaEste ='$medidaEste', medidaOeste ='$medidaOeste'  WHERE id_lote ='$id_lote'";
$result = $mysqli->query($query);
						$mysqli->close();
						return new soapval('return','xsd:string', 'Medidas Registradas con exito');
}

function getNumeroLote($id_lote){
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}
	$idLote = (int) $id_lote;
	$query = "SELECT n_lote FROM Lote WHERE id_lote = '$idLote'" ;
	$result = $mysqli->query($query);
	$data = $result->fetch_assoc();
	$numero_lote = $data['n_lote'];
	$mysqli->close();
	return new soapval('return','xsd:string', $numero_lote);

}

function getStatusLotes($pk_predio){
$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}

	$query = "SELECT * FROM `Lote` WHERE `pk_predio` = '$pk_predio' AND `status_lote` = '0'";
	$result = $mysqli->query($query);
	$numeroLotesDisp = $result->num_rows;
	return new soapval('return','xsd:string', $numeroLotesDisp);
}

function contarLotes($pk_manzana, $pk_predio){
$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}

	$idManzana = (int)$pk_manzana;
	if($idManzana>0){
	$query = "SELECT * FROM Lote WHERE pk_manzana = '$pk_manzana' AND pk_predio = '$pk_predio'";
		$result = $mysqli->query($query);
		$numero = $result->num_rows;
	}
	else{
		$query = "SELECT * FROM Lote WHERE pk_predio = '$pk_predio' AND pk_manzana = '0'";
		$result = $mysqli->query($query);
		$numero = $result->num_rows;
	}
		return new soapval('return','xsd:string', $numero);
}

function getMedidaLote($id_lote){
$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}

$query = "SELECT `medidaNorte`, `medidaSur`, `medidaEste`, `medidaOeste`  FROM `Lote` WHERE `id_lote` = '$id_lote'";
	$result = $mysqli->query($query);
	$data = $result->fetch_assoc();
	$medidaNorte = $data['medidaNorte'];
	$medidaSur = $data['medidaSur'];
	$medidaEste =$data['medidaEste'];
	$medidaOeste = $data['medidaOeste'];
	$medidas = $medidaNorte.",".$medidaSur.",".$medidaEste.",".$medidaOeste;
	$mysqli->close();
	return new soapval('return','xsd:string', $medidas);
}

function updateStatusLote($id_lote,$statusLote)
{
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}
	//$numero_lote = (int) $numero_lote;
	$query = "UPDATE Lote SET status_lote = '$statusLote' WHERE  id_lote = '$id_lote'" ;
	$mysqli->query($query);
	$echo = $mysqli->affected_rows;
	if ($echo == 1) {
			return new soapval('return','xsd:string', "ya esta disponible");
	}else{
			return new soapval('return','xsd:string', "No hubo cambio");
	}
}

function eliminaLote($id_sesion, $id_lote){
	$sesion = validaSesion($id_sesion);
	if($sesion != null){
		$id_usuario = $sesion->getId_user();
		$usuario = $sesion->getUsuario();
		if($usuario->getTipo() == 1){
			return new soapval('return','xsd:string', 'IN');
		}else{
			return new soapval('return','xsd:string', 'NO TIENE ACCESO');
		}
	}else{
		return new soapval('return','xsd:string','NO TIENE ACCESO');
	}
}

function buscaLotePor($id_sesion, $modo, $valor){
	$sesion = validaSesion($id_sesion);
	if($sesion != null){
		$id_usuario = $sesion->getId_user();
		$usuario = $sesion->getUsuario();
		if($usuario->getTipo() == 1){
			return new soapval('return','xsd:string', 'IN');
		}else{
			return new soapval('return','xsd:string', 'NO TIENE ACCESO');
		}
	}else{
		return new soapval('return','xsd:string','NO TIENE ACCESO');
	}
}

?>