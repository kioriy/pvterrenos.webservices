<?php
function registraPredio($id_predio, $nombre_predio, $colonia, $municipio,$administracion)
{
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}
	$query = "INSERT INTO `Predio`(`id_predio`, `nombre_predio`, `colonia`, `municipio`,`administracion`) VALUES ('$id_predio', '$nombre_predio', '$colonia', '$municipio','$administracion')";
						$result = $mysqli->query($query);
						$mysqli->close();
						return new soapval('return','xsd:string', 'Predio registrado con EXITO');


	/*$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	//$password = sha1($password);

	$query ="SELECT id_user, UUID() AS id_sesion FROM Usuario WHERE email = '$email' AND password='$password'";
	if ($mysqli->connect_errno) {
		return new soapval('return','xsd:string','NO TIENE ACCESO');
	}else{
		$result = $mysqli->query($query);
		$i = $result->num_rows;
		if($i == 1){
		//if($i>0){
			$data = $result->fetch_assoc();
		    $id_sesion = $data['id_sesion'];
		    $id_user = $data['id_user'];
		    $query ="DELETE FROM Sesiones WHERE (fk_usuario = $id_user)";
		    $result = $mysqli->query($query);
		    $query = "INSERT INTO Sesiones (fk_usuario, id_sesion, fecha_sesion )VALUES ( $id_user, '$id_sesion', CURDATE())";
		    $result = $mysqli->query($query);
			$mysqli->close();
			return new soapval('return','xsd:string', $id_sesion);
		}else{
			$mysqli->close();
	    		return new soapval('return','xsd:string','USUARIO NO REGISTRADO');
		}
	}*/
}

function cargaColumnaTablaPredio($nombreColumna){

	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}
	$query = "SELECT $nombreColumna FROM `Predio` ORDER BY `Predio`.$nombreColumna ASC" ;
						$datos = array();
						$result = $mysqli->query($query);
						while ( $data = $result->fetch_assoc()) {
						    $datos[] = $data["$nombreColumna"];
						};
						$columna = implode(",",$datos);
						$mysqli->close();
						return new soapval('return','xsd:string', $columna);
}

function getPredio($nombrePredio){
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli_connect_errno){
		printf("conexion fallida %s/n",$mysqli_connect_errno);
		exit();
	}
	$query = "SELECT * FROM `Predio` WHERE `nombre_predio` = '$nombrePredio'" ;
						$result = $mysqli->query($query);
						$data = $result->fetch_assoc();
		    			$idPredio=$data['id_predio'];
		    			$colonia=$data['colonia'];
		    			$municipio=$data['municipio'];
						$mysqli->close();
						return new soapval('return','xsd:string',"$idPredio,$colonia,$municipio");

}

function getDatoPredio($columna, $where, $igual){

	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli_connect_errno){
		printf("conexion fallida %s/n",$mysqli_connect_errno);
		exit();
	}
	$query = "SELECT $columna FROM `Predio` WHERE $where = '$igual'" ;
	$result = $mysqli->query($query);
	$data = $result->fetch_assoc();
	$dato = $data["$columna"];
	$mysqli->close();
	return new soapval('return','xsd:string', $dato);

}

function getIdPredio($nombre_predio)
{
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}
	$query = "SELECT id_predio FROM `Predio` WHERE nombre_predio = '$nombre_predio'" ;
	$result = $mysqli->query($query);
	$data = $result->fetch_assoc();
	$id_predio = $data['id_predio'];
	$mysqli->close();
	return new soapval('return','xsd:string', $id_predio);
}

function getinfoPredio(){
$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno)
	{
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}
	$query = "SELECT `nombre_predio`,id_predio FROM `Predio` ORDER BY `Predio`.`nombre_predio` ASC";
						$datos = array();
						$result = $mysqli->query($query);

						while ( $data = $result->fetch_assoc()) {
						    $nombre = $data['nombre_predio'];
						    $idPredio =  $data['id_predio'];
						    $queryManzana = "SELECT * FROM Manzana WHERE pk_predio = '$idPredio'";
							$resultManzana = $mysqli->query($queryManzana);
						    $numeroManza = $resultManzana->num_rows;

						    $queryLotes = "SELECT * FROM `Lote` WHERE `pk_predio` = '$idPredio'";
							$resultLotes = $mysqli->query($queryLotes);
							$numeroLotes = $resultLotes->num_rows;

						   $queryLotesDisponibles = "SELECT * FROM `Lote` WHERE `pk_predio` = '$idPredio' AND `status_lote` = '0'";
						   $resultLotesDisponibles = $mysqli->query($queryLotesDisponibles);
						   $numeroLotesDisponibles = $resultLotesDisponibles->num_rows;


						    $datos[] = $nombre.",".$numeroManza.",".$numeroLotes.",".$numeroLotesDisponibles;
						};
						$DatosPredio = implode("|",$datos);
						$mysqli->close();
						return new soapval('return','xsd:string', $DatosPredio);

}

function getIdPredioPkLote($pk_lote){
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli_connect_errno){
		printf("conexion fallida %s/n",$mysqli_connect_errno);
		exit();
	}
	$pkLote = (int) $pk_lote;
	$query = "SELECT `pk_predio` FROM `Lote` WHERE `id_lote` = '$pkLote' " ;
	$result = $mysqli->query($query);
	$data = $result->fetch_assoc();
	$id_predio = $data['pk_predio'];
	$mysqli->close();
	return new soapval('return','xsd:string', $id_predio);
}

function getNombrePredio($id_predio){
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli_connect_errno){
		printf("conexion fallida %s/n",$mysqli_connect_errno);
		exit();
	}
	$query = "SELECT nombre_predio FROM `Predio` WHERE id_predio = '$id_predio'" ;
	$result = $mysqli->query($query);
	$data = $result->fetch_assoc();
	$nombre_predio = $data['nombre_predio'];
	$mysqli->close();
	return new soapval('return','xsd:string', $nombre_predio);
}

function eliminaPredio($id_sesion){

}

function getAllPredio($idLote)
{
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}

	////Query id manzana
	$queryIdManzana = "SELECT `pk_manzana` FROM `Lote` WHERE `id_lote` = '$idLote' " ;

	$resultIdManzana = $mysqli->query($queryIdManzana);
	$dataIdManzana = $resultIdManzana->fetch_assoc();

	$idManzana = $dataIdManzana['pk_manzana'];
	////fin

	////Query numero de manzana
	$queryNumeroManzana = "SELECT n_manzana FROM Manzana WHERE id_manzana = '$idManzana' " ;

	$resultNumeroManzana = $mysqli->query($queryNumeroManzana);
	$dataNumeroManzana = $resultNumeroManzana->fetch_assoc();

	$numero_manzana = $dataNumeroManzana['n_manzana'];
	////fin

	////Query id predio
	$queryIdPredio = "SELECT `pk_predio` FROM `Lote` WHERE `id_lote` = '$idLote' " ;

	$resultIdPredio = $mysqli->query($queryIdPredio);
	$dataIdPredio = $resultIdPredio->fetch_assoc();

	$id_predio = $dataIdPredio['pk_predio'];
	////fin

	////Query nombre predio
	$queryNombrePredio = "SELECT nombre_predio FROM `Predio` WHERE id_predio = '$id_predio'" ;

	$resultNombrePredio = $mysqli->query($queryNombrePredio);
	$dataNombrePredio = $resultNombrePredio->fetch_assoc();

	$nombre_predio = $dataNombrePredio['nombre_predio'];
	////fin

	////Query numero lote
	$queryNumeroLote = "SELECT n_lote FROM Lote WHERE id_lote = '$idLote'" ;

	$resultNumeroLote = $mysqli->query($queryNumeroLote);
	$dataNumeroLote = $resultNumeroLote->fetch_assoc();

	$numero_lote = $dataNumeroLote['n_lote'];
	////fin

	$mysqli->close();

	return new soapval('return','xsd:string', "$nombre_predio,$numero_manzana,$numero_lote");
}
?>
