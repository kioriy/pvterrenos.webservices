<?php
class SesionClass{
		private $id_sesion;
		private $id_user;
		private $date_sesion;
		private $usuario;
		function __construct($id_user, $id_sesion, $date_sesion, $usuario) {
	        $this->id_user = $id_user;
	        $this->id_sesion = $id_sesion;
	        $this->date_sesion = $date_sesion;
	        $this->usuario = $usuario;
	   	}
	   	public function getId_sesion(){
	  		return $this->id_sesion;
	   	}
	   	public function getId_user(){
	   		return $this->id_user;
	 	}
	   	public function getDate_sesion(){
	   		return $this->date_sesion;
	 	}
	 	public function getUsuario(){
	 		return $this->usuario;
	 	}
}

class Usuario{
	private $id_user;
	private $nombre;
	private $apellidoPaterno;
	private $apellidoMaterno;
	private $tipo;
	private $email;
	function __construct($id_user, $nombre, $apellidoPaterno, $apellidoMaterno, $tipo, $email){
		$this->id_user = $id_user;
		$this->nombre = $nombre;
		$this->apellidoPaterno = $apellidoPaterno;
		$this->apellidoMaterno = $apellidoMaterno;
		$this->tipo = $tipo;
		$this->email = $email;
	}
	public function getId_user(){
		return $this->id_user;
	}
	public function getNombre(){
		return $this->nombre;
	}
	public function getApellidoPaterno(){
		return $this->apellidoPaterno;
	}
	public function getApellidoMaterno(){
		return $this->apellidoMaterno;
	}
	public function getTipo(){
		return $this->tipo;
	}
	public function getEmail(){
		return $this->email;
	}
}

class ClassComprador{
	private $id;
	private $nombre;
	private $direccion;
	private $beneficiario;
	private $residencia;
	private $ocupacion;
	private $estado;
	private $tel;
	private $tel1;
	function __construct($id, $nombre, $direccion, $beneficiario, $residencia, $ocupacion, $estado, $tel, $tel1){
		$this->id = $id;
		$this->nombre = $nombre;
		$this->direccion = $direccion;
		$this->beneficiario = $beneficiario;
		$this->residencia = $residencia;
		$this->ocupacion = $ocupacion;
		$this->estado = $estado;
		$this->tel = $tel;
		$this->tel1 = $tel1;
	}

	function getId(){
		return $this->id;
	}

	function getNombre(){
		return $this->nombre;
	}

	function getDireccion(){
		return $this->direccion;
	}

	function getBeneficiario(){
		return $this->beneficiario;
	}

	function getResidencia(){
		return $this->residencia;
	}

	function getOcupacion(){
		return $this->ocupacion;
	}

	function getEstado(){
		return $this->estado;
	}
	function getTel(){
		return $this->tel;
	}

	function getTel1(){
		return $this->tel1;
	}
}

class ClassCompraVenta{
	private $id_venta;
	private $fk_idcomprador;
	private $fk_idlote;
	private $tipo_pago;
	private $monto;
	private $plazo;
	private $fecha_corte;
	private $fecha_compra;

	function __construct($id_venta, $fk_idcomprador,$id_lote, $tipo_pago, $monto, $plazo, $fecha_corte, $fecha_compra){
		$this->id_venta = $id_venta;
		$this->fk_idcomprador = $fk_idcomprador;
		$this->fk_idlote = $fk_idlote;
		$this->tipo_pago = $tipo_pago;
		$this->monto = $monto;
		$this->plazo = $plazo;
		$this->fecha_corte = $fecha_corte;
		$this->fecha_compra = $fecha_compra;
	}
	function getIdVenta(){
		return $this->id_venta;
	}
	function getFkIdComprador(){
		return $this->fk_idcomprador;
	}
	function getFkIdLote(){
		return $this->fk_idlote;
	}
	function getTipoPago(){
		return $this->tipo_pago;
	}
	function getMonto(){
		return $this->monto;
	}
	function getPlazo(){
		return $this->plazo;
	}
	function getFechaCorte(){
		return $this->fecha_corte;
	}
	function getFechaCompra(){
		return $this->fecha_compra;
	}
}


class ClassPago{

}

class ClassPredio{

	private $id_predio;
	private $nombre_predio;

	function __construct($id_predio, $nombre_predio){
		$this->id_predio = $id_predio;
		$this->nombre_predio = $nombre_predio;
	}

	function getId(){
		return $this->id_predio;
	}

	function getNombre(){
		return $this->nombre_predio;
	}

}

class ClassLote{
	private $id_lote;
	private $numero_lote;
	private $pk_idmanzana;
	private $pk_idpredio;

	function __construct($id_lote, $numero_lote, $pk_idmanzana, $pk_idpredio){
		$this->id_lote = $id_lote;
		$this->numero_lote = $numero_lote;
		$this->pk_idmanzana = $pk_idmanzana;
		$this->pk_idpredio = $pk_idpredio;
	}
	function getIdLote(){
		return $this->id_lote;
	}
	function getNumeroLote(){
		return $this->numero_lote;
	}
	function getPkidManzana(){
		return $this->pk_idmanzana;
	}
	function getPkidPredio(){
		return $this->pk_idpredio;
	}
}

class ClassManzana{
	private $id_manzana;
	private $numero_manzana;
	private $pk_idpredio;

	function __construct($id_manzana, $numero_manzana, $pk_idpredio){
		$this->id_manzana = $id_manzana;
		$this->numero_manzana = $numero_manzana;
		$this->pk_idpredio = $pk_idpredio;
	}
	function getIdManzana(){
		return $this->id_manzana;
	}
	function getNumeroManzana(){
		return $this->numero_manzana;
	}
	function getPKidPredio(){
		return $this->pk_idpredio;
	}

}

/*class ClassReporte{
	private $fecha;
	private $nombre;
	private $pago;
	private $predio;
	private $lote;
	private $manzana;

	function __construct($fecha, $nombre, $pago, $predio, $lote, $manzana){
		$this-> $fecha = $fecha;
		$this-> $nombre = $nombre;
		$this-> $pago = $pago;
		$this-> $predio = $predio;
		$this-> $lote = $lote;
		$this-> $manzana = $manzana;


	}


}*/

class ClassRegistroPublicoPredio	{

}

?>