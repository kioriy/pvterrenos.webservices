<?php


function reportePago($numeroPAgo, $fraccionamiento, $numeroLote, $medidaN, $medidaS, $medidaE, $medidaO, $numeroManzana,$cliente,$monto,$fechaPago,$municipio){
include_once('fpdf.php');


$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial','B',12);
//PARA SACAR EL MES DE LA FECHA
$mes = mb_strtoupper (strftime("%B", strtotime ($fechaPago)));

//METROS CUADRADOS
$medidaTotal = ((int)$medidaN * (int)$medidaS);

$escrito1 = '         RECIBI DEL ';
$escrito2 = 'LA CANTIDAD DE';
$escrito3 = 'POR CONCEPTO DE PAGO DEL MES DE';
$escrito4 = 'DEL LOTE No.';
$escrito5 = ' DE LA MANZANA';
$escrito6 = ',UBICADO EN EL "FRACCIONAMIENTO",';
$escrito7 = ',MUNICIPIO DE';
$escrito8 = ',DICHO LOTE TIENE UNA MEDIDA DE';
$escrito9 = 'POR';
$escrito10 = 'MTS SIENDO UN TOTAL DE';
$escrito11 = 'MTS CUADRADOS.';

$escrito = $escrito1.' '.$cliente.'  '.$escrito2.' '.$monto.'  '.$escrito3.' '.$mes.' '.$escrito4.' '.$numeroLote.' '.$escrito5.' '.$numeroManzana.' '.$escrito6;
$recibo = $escrito.' '.$fraccionamiento.' '.$escrito7.' ' .$municipio.' '.$escrito8.' '.$medida1.' '.$escrito9.' '.$medida2.' '.$escrito10.' '.$medidaTotal.' '.$escrito11;


//varible para poner numero de pago
$pago1 = 'PAGO NO.';
$pago12= 'DE';
$pago = $pago1.'    '.$pago2;

//variable para numero de lote y manzana
$Nlote = 'LOTE No.';
$Nmanzana = 'MANZANA';
$loteManzana = $Nlote.'   '.$Nmanzana;


//numero de pago
$pdf->SetY(36);
$pdf->SetX(38);
$pdf->Cell(0,3,$pago);

//MUNICIPIO
$pdf->SetY(50);
$pdf->SetX(38);
$pdf->Cell(0,3,'MUNICIPIO','C');

//NUMERO DE LOTE  MANZANA
$pdf->SetY(36);
$pdf->SetX(125);
$pdf->Cell(0,3,$loteManzana,'C');

//FECHA
$pdf->SetY(50);
$pdf->SetX(125);
$pdf->Cell(0,3,'FECHA','C');


//escrito
$pdf->SetY(70);
$pdf->SetX(38);
$pdf->Multicell(140,5,$recibo,0);

///VENDEDOR
$pdf->SetY(128);
$pdf->SetX(99);
$pdf->Cell(80,3,'SR MOISES SANTIAGO FIGUEROA',0,0,'C');

$pdf->SetY(136);
$pdf->SetX(99);
$pdf->Cell(80,3,'VENDEDOR',0,0,'C');


$pdf->Output('prueba.pdf','D');


//$regresa = $pdf->Output('reporte','S');
return new soapval('return','xsd:string', "aqui estube");

}

?>