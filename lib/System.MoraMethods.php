<?php

function registraMora($id_venta, $monto_mora, $fecha_pago, $mes_mora, $fecha_mora, $status_mora){
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}
	$idVenta = (int) $id_venta;
	$query = "INSERT INTO `Mora`(`pk_venta`, `monto_mora`, `fecha_pago`, `mes_mora`, `fecha_mora`, `status`) VALUES ('$idVenta', '$monto_mora', '$fecha_pago', '$mes_mora', '$fecha_mora', '$status_mora')";
	$result = $mysqli->query($query);
	$mysqli->close();
	return new soapval('return','xsd:string', 'Registro de mora EXITOSA');

}

function getIdMora($id_venta){

	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}
	$idVenta = (int)$id_venta;
	$query = "SELECT `id_mora` FROM `Mora` WHERE `pk_venta` = '$idVenta' AND status = '0' ORDER BY `Mora`.`mes_mora` ASC" ;
	$datos = array();
	$result = $mysqli->query($query);
	while ( $data = $result->fetch_assoc()) {
		$datos[] = $data['id_mora'];
	};
	$idMora = implode(",",$datos);
	$mysqli->close();
	return new soapval('return','xsd:string', $idMora);

}

function getFechaMora($id_venta){

	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}
	//AND status = '0'
	$idVenta = (int) $id_venta;
	$query = "SELECT mes_mora FROM Mora WHERE pk_venta = '$idVenta' AND `status` = '0' ORDER BY `Mora`.`mes_mora` ASC";
	$datos = array();
	$result = $mysqli->query($query);
	while ( $data = $result->fetch_assoc()) {
		$datos[] = $data['mes_mora'];
	};
	$mesMora = implode(",",$datos);
	$mysqli->close();
	return new soapval('return','xsd:string', $mesMora);

}

function getUltimoMes($id_venta){

	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}
	$idVenta = (int) $id_venta;
	$query = "SELECT mes_mora FROM Mora WHERE pk_venta = '$idVenta' ORDER BY `Mora`.`mes_mora` DESC";
	$datos = array();
	$result = $mysqli->query($query);
	while ( $data = $result->fetch_assoc()) {
		$datos[] = $data['mes_mora'];
	};
	$mesMora = implode(",",$datos);
	$mysqli->close();
	return new soapval('return','xsd:string', $mesMora);

}

function getMontoMora($id_venta){
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}
	$idVenta = (int) $id_venta;
	$query = "SELECT monto_mora FROM Mora WHERE pk_venta = '$idVenta' AND status = '0' ORDER BY `Mora`.`mes_mora` ASC";
	$datos = array();
	$result = $mysqli->query($query);
	while ( $data = $result->fetch_assoc()) {
		$datos[] = $data['monto_mora'];
	};
	$montoMora = implode(",",$datos);
	$mysqli->close();
	return new soapval('return','xsd:string', $montoMora);
}

function updateTablaMora($id_mora,$dato,$nombreColumna){

	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}

	$idMora = explode(",",$id_mora);
	$tama�o = count($idMora);
//	return new soapval('return','xsd:string', $tama�o);
	for($i=0; $i<$tama�o; $i++){
//		$i=1;
		$query = "UPDATE `Mora` SET $nombreColumna = '$dato' WHERE `id_mora` = '$idMora[$i]'" ;
		$mysqli->query($query);

	}
	$echo = $mysqli->affected_rows;
	if ($echo == 1) {
		return new soapval('return','xsd:string', "1");
	}else{
		return new soapval('return','xsd:string', "No hubo cambio");
	}
}

function actualizaMontoMora($idMora,$monto){

	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}
	$id_Mora = (int) $idMora;
	$query = "UPDATE `Mora` SET `monto_mora`= '$monto'  WHERE `id_mora` = '$id_Mora'" ;
	$mysqli->query($query);
	$echo = $mysqli->affected_rows;
	if ($echo == 1) {
		return new soapval('return','xsd:string', "Status 1");
	}else{
		return new soapval('return','xsd:string', "No hubo cambio");
	}
}

function getMorosos($fecha)
{
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);

	if ($mysqli->connect_errno)
	{
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}

	$query = "SELECT `pk_venta` FROM `ProximoPago` WHERE DATE_FORMAT(STR_TO_DATE( `proximo_pago` , '%d/%m/%Y' ), '%m/%Y') =
	                                                     DATE_FORMAT(STR_TO_DATE( '$fecha', '%d/%m/%Y'), '%m/%Y')
	                                                     ORDER BY  `pk_venta` ASC";

	$idVenta = array();
	$datos = array();
	$nombreComprador;
	$telefono1;
	$telefono2;

	$result = $mysqli->query($query);

	if ($result)
	{
		while ($data = $result->fetch_assoc())
		{
			$idVenta[] = $data['pk_venta'];
		};

		$tama�o = count($idVenta);


		if ($result->num_rows > 0)
		{
			$result->close();

			for($i = 0; $i < $tama�o; $i++)
			{
				$query = "SELECT `nombre`, `tel1`, `tel2` FROM `Comprador` WHERE `id_comprador` =
		             (SELECT `pk_comprador` FROM `CompraVenta` WHERE  `id_venta` = $idVenta[$i] AND `status_venta` = 'ACTIVO') COLLATE utf8_general_ci";

				$result = $mysqli->query($query);

				if ($result->num_rows > 0)
				{
					$data = $result->fetch_assoc();

					$nombreComprador = $data['nombre'];
					$telefono1 = $data['tel1'];
					$telefono2 = $data['tel2'];

					$datos[] = $nombreComprador.",".$telefono1.",".$telefono2;
				}
				//$clientesMorosos = implode("|",$idVenta);
				//return new soapval('return','xsd:string', $clientesMorosos." ".$nombreComprador." ".$telefono1." ".$datos[0]);
			}
			$result->close();
			$mysqli->close();
			$clientesMorosos = implode("|",$datos);

			return new soapval('return','xsd:string', $clientesMorosos);
		}
		else
		{
			return new soapval('return','xsd:string', "0");
		}

		/*$filasAfectadas = $mysqli->affected_rows;

		if ($filasAfectadas > 0)
		{
			return new soapval('return','xsd:string', $clientesMorosos);
		}
		else
		{
			return new soapval('return','xsd:string', "La consulta no genero ningun resultado");
		}*/
	}
	else
	{
		return new soapval('return','xsd:string', "Error al ejecutar la consulta");
	}
}

function getDatosMora($query)
{
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);

	if ($mysqli->connect_errno)
	{
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}

	//$query = "SELECT mes_mora FROM Mora WHERE pk_venta = '$idVenta' ORDER BY `Mora`.`mes_mora` DESC";

	$datos = array();
	$result = $mysqli->query($query);

	if($result)
	{
		if ($mysqli->affected_rows > 0)
		{
			while ($data = $result->fetch_assoc())
			{
				$datos[] = $data['mes_mora'];
			};

			$mesMora = implode(",",$datos);
			$mysqli->close();

			return new soapval('return','xsd:string', $mesMora);
		}
		else
		{
			return new soapval('return','xsd:string', "0");
		}

	}
	else
	{
		return new soapval('return','xsd:string', "No se realizo la consulta");
	}
}

?>