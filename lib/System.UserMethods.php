<?php
function login($email, $password) {
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	//$password = sha1($password);

	$query = "SELECT `nombre`,`tipo`,`id_user`,`Administracion` FROM `Usuario` WHERE `nombre` = '$email' and `password` = '$password'";
		//"SELECT id_user, UUID() AS id_sesion FROM Usuario WHERE email = '$email' AND password='$password'";
	if ($mysqli->connect_errno) {
		return new soapval('return','xsd:string','NO TIENE ACCESO');
	}else{
		$result = $mysqli->query($query);
		$i = $result->num_rows;
		if($i == 1){
		//if($i>0){
			$data = $result->fetch_assoc();
		  $nombre = $data['nombre'];
		  $tipo = $data['tipo'];
			$idUser = $data['id_user'];
			$Administracion = $data['Administracion'];
		    //$query ="DELETE FROM Sesiones WHERE (fk_usuario = $id_user)";
		    //$result = $mysqli->query($query);
		    //$query = "INSERT INTO Sesiones (fk_usuario, id_sesion, fecha_sesion )VALUES ( $id_user, '$id_sesion', CURDATE())";
		    //$result = $mysqli->query($query);
			$mysqli->close();
			return new soapval('return','xsd:string', $nombre.",".$tipo.",".$idUser.",".$Administracion);
		}else{
			$mysqli->close();
	    		return new soapval('return','xsd:string','USUARIO NO REGISTRADO');
		}
	}
}

function registraUsuario($id_sesion, $nombre, $apellidoPaterno, $apellidoMaterno, $tipo, $email, $password ){
		$password = sha1($password);
		$sesion = validaSesion($id_sesion);
		if($sesion != null){
			$id_usuario = $sesion->getId_user();
			$usuario = $sesion->getUsuario();
			if($usuario->getTipo() == 1){
				$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
				$query ="SELECT id_user FROM Usuario WHERE (email ='$email')";
				if ($mysqli->connect_errno) {
				    return new soapval('return','xsd:string','no se puede crear el usuario nuevo');
				}else{
					$result = $mysqli->query($query);
					$i = $result->num_rows;
					if($i == 0){
						$query = "INSERT INTO Usuario (id_user, nombre, apellidoPaterno,apellidoMaterno, tipo, email, password) VALUES ( NULL ,  '$nombre',  '$apellidoPaterno',  '$apellidoMaterno',  '$tipo',  '$email',  '$password')";
						$result = $mysqli->query($query);
						$mysqli->close();
						return new soapval('return','xsd:string', 'OKAY');
					}else{
						$mysqli->close();
						return new soapval('return','xsd:string', 'Usuario ya registrado');
					}
				}
			}else{
				return new soapval('return','xsd:string', 'NO TIENE ACCESO');
			}
		}else{
			return new soapval('return','xsd:string','NO TIENE ACCESO');
		}
}

function listarUsuarios($id_sesion){
	$sesion = validaSesion($id_sesion);
	if($sesion != null){
		$id_usuario = $sesion->getId_user();
		$usuario = $sesion->getUsuario();
		if($usuario->getTipo() == 1){
				$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
				$query ="SELECT * FROM Usuario";
				if ($mysqli->connect_errno) {
				    return new soapval('return','xsd:string','no se puede crear el usuario nuevo');
				}else{
					$result = $mysqli->query($query);
					$arrayJSON=array();
					$i=0;
					while($data = $result->fetch_assoc()){
						$arrayJSON[$i]['id_user'] = $data['id_user'];
						$arrayJSON[$i]['nombre'] = $data['nombre'];
						$arrayJSON[$i]['apellidoPaterno'] = $data['apellidoPaterno'];
						$arrayJSON[$i]['apellidoMaterno'] = $data['apellidoMaterno'];
						$arrayJSON[$i]['tipo'] = $data['tipo'];
						$arrayJSON[$i]['email'] = $data['email'];
						$i++;
					}
					$mysqli->close();
					return new soapval('return','xsd:string',json_encode($arrayJSON));
				}
		}else{
			return new soapval('return','xsd:string', 'NO TIENE ACCESO');
		}
	}else{
		return new soapval('return','xsd:string','NO TIENE ACCESO');
	}
}

function usuarioUpdate($id_sesion, $id_usuario, $nombre, $apellidoPaterno, $apellidoMaterno, $tipo, $email){
	$sesion = validaSesion($id_sesion);
	if($sesion != null){
		$usuario = $sesion->getUsuario();
		if($usuario->getTipo() == 1 && idUserExist($id_usuario)){
			if(validaEmail($email) || $email == "N/A"){
				if(!emailExist($email)){
					$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
					if ($mysqli->connect_errno) {
				  	return new soapval('return','xsd:string','NO HAY ACCESO');
					}else{
						if($nombre != "" && $apellidoPaterno != "" && $apellidoMaterno != "" && $tipo != "" && $email != ""){
							if($email == "N/A"){
								$query ="UPDATE Usuario SET  nombre =  '$nombre', apellidoPaterno =  '$apellidoPaterno', apellidoMaterno =  '$apellidoMaterno', tipo =  $tipo WHERE  id_user =$id_usuario;";
								$mysqli->query($query);
								$echo = $mysqli->affected_rows;
								if($echo ==1){
									$mysqli->close();
									return new soapval('return','xsd:string', "OKAY");
								}else{
									$mysqli->close();
									return new soapval('return','xsd:string', "ERROR");
								}
							}else{
								$query ="UPDATE Usuario SET  nombre =  '$nombre', apellidoPaterno =  '$apellidoPaterno', apellidoMaterno =  '$apellidoMaterno', tipo =  $tipo, email =  '$email' WHERE  id_user =$id_usuario";
								$mysqli->query($query);
								$echo = $mysqli->affected_rows;
								if($echo ==1){
									$mysqli->close();
									return new soapval('return','xsd:string', "OKAY");
								}else{
									$mysqli->close();
									return new soapval('return','xsd:string', "ERROR");
								}
							}
						}else{
							return new soapval('return','xsd:string', "hay campos sin datos");
						}
					}
				}else{
					return new soapval('return','xsd:string', "Error Email en uso");
				}
			}else{
				return new soapval('return','xsd:string', "EMAIL NO VALIDO");
			}

		}else{
			return new soapval('return','xsd:string', 'NO TIENE ACCESO Usuario no valido');
		}
	}else{
		return new soapval('return','xsd:string','NO TIENE ACCESO');
	}
}

function usuarioChangePassword($id_sesion, $id_usuario, $password){
	$sesion = validaSesion($id_sesion);
	if($sesion != null){
		$usuario = $sesion->getUsuario();
		if($usuario->getId_user()==$id_usuario || $usuario->getTipo() == 1){

			return new soapval('return','xsd:string', 'puede modificar');
		}else{
			return new soapval('return','xsd:string', 'NO TIENE ACCESO');
		}
	}else{
		return new soapval('return','xsd:string','NO TIENE ACCESO');
	}
}

function getPerfilUsuario($id_sesion){
	$sesion = validaSesion($id_sesion);
	if($sesion != null){
		$id_usuario = $sesion->getId_user();
		$usuario = $sesion->getUsuario();
		$id_usuario = $usuario->getId_user();
		$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
		$query ="SELECT * FROM Usuario WHERE id_user = '$id_usuario'";
		if ($mysqli->connect_errno) {
				    return new soapval('return','xsd:string','no se puede crear el usuario nuevo');
		}else{
			$result = $mysqli->query($query);
			$i = $result->num_rows;
			if($i == 1){
				$arrayJSON=array();
				$data = $result->fetch_assoc();
				$arrayJSON['id_user'] = $data['id_user'];
				$arrayJSON['nombre'] = $data['nombre'];
				$arrayJSON['apellidoPaterno'] = $data['apellidoPaterno'];
				$arrayJSON['apellidoMaterno'] = $data['apellidoMaterno'];
				$arrayJSON['tipo'] = $data['tipo'];
				$arrayJSON['email'] = $data['email'];
				$mysqli->close();
				return new soapval('return','xsd:string', json_encode($arrayJSON));
			}else{
				$mysqli->close();
				return new soapval('return','xsd:string','NO TIENE ACCESO');
			}
		}
		return new soapval('return','xsd:string', 'NO TIENE ACCESO');
	}else{
		return new soapval('return','xsd:string','NO TIENE ACCESO');
	}
}

function buscarUsuarioPor($id_sesion, $modo, $valor){//NOMBE, PREDIO, id,
	$sesion = validaSesion($id_sesion);
	if($sesion != null){
		$id_usuario = $sesion->getId_user();
		$usuario = $sesion->getUsuario();
		switch($modo){
			case 'NOMBRE'://nombre
			if(strlen($valor)<3)return new soapval('return','xsd:string','ERROR');
			$query = "SELECT * FROM Usuario WHERE (nombre LIKE  '%".$valor."%')";
			$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
				if ($mysqli->connect_errno) {
				    return new soapval('return','xsd:string','ERROR');
				}else{
					$result = $mysqli->query($query);
					$arrayJSON=array();
					$i=0;
					while($data = $result->fetch_assoc()){
						$arrayJSON[$i]['id_user'] = $data['id_user'];
						$arrayJSON[$i]['nombre'] = $data['nombre'];
						$arrayJSON[$i]['apellidoPaterno'] = $data['apellidoPaterno'];
						$arrayJSON[$i]['apellidoMaterno'] = $data['apellidoMaterno'];
						$arrayJSON[$i]['tipo'] = $data['tipo'];
						$arrayJSON[$i]['email'] = $data['email'];
						$i++;
					}
					$mysqli->close();
					return new soapval('return','xsd:string',json_encode($arrayJSON));
				}
			break;

			case 'APELLIDO'://apellido
				return new soapval('return','xsd:string','B');
			break;

			case 'C'://id
			return new soapval('return','xsd:string','C');
			break;

			case 'D'://venta
			return new soapval('return','xsd:string','D');
			break;

			default:
			return new soapval('return','xsd:string','OPCION NO VALIDA');
		}

	}else{
		return new soapval('return','xsd:string','NO TIENE ACCESO');
	}
}

function registroActividad($usuario, $idActividad, $nombreActividad, $fecha)
{
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);

	if ($mysqli->connect_errno)
	{
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}

	$query = "INSERT INTO `RegistroActividad`(`nombreUsuario`, `idActividad`, `nombreActividad`, `fecha`)
	               VALUES ('$usuario','$idActividad','$nombreActividad','$fecha')";

	$result = $mysqli->query($query);

	$echo = $mysqli->affected_rows;

	$mysqli->close();

	if($echo == 1)
	{
		return new soapval('return','xsd:string', "Se registro actividad del usuario");
	}
	else
	{
		return new soapval('return','xsd:string', "fallo");
	}
}

?>
