<?php
//require('System.PrediosMethods.php');
//require('System.ManzanasMethods.php');

function registraProximoPago($id_venta, $monto, $proximo_pago, $pago_actual, $pago_final){

	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);

	if ($mysqli->connect_errno)
	{
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}

	$query = "INSERT INTO `ProximoPago`(`pk_venta`, `monto`, `proximo_pago`, `pago_actual`, `pago_final`) VALUES ($id_venta, '$monto', '$proximo_pago', '$pago_actual', '$pago_final')";
	$result = $mysqli->query($query);

	$filasAfectadas = $mysqli->affected_rows;

	$mysqli->close();

	if($filasAfectadas > 0)
	{
			return new soapval('return','xsd:string', "Se registro proximo pago");
	}
	else
	{
	     	return new soapval('return','xsd:string', "No se realizo el registro");
	}


//	$sesion = validaSesion($id_sesion);
//	if($sesion != null){
//		$id_usuario = $sesion->getId_user();
//		$usuario = $sesion->getUsuario();
//		if($usuario->getTipo() == 1){
//			return new soapval('return','xsd:string', 'IN');
//		}else{
//			return new soapval('return','xsd:string', 'NO TIENE ACCESO');
//		}
//	}else{
//		return new soapval('return','xsd:string','NO TIENE ACCESO');
//	}

}

function registraAllPago($idMora, $datoStatusMora, $nombreColumna,//datos para el registro de moras
	                     $id_venta, $monto, $montoP, $fecha_pago, $fecha_corte, $tipo_pago, $pago_actual,$montoPagado,//datos para registro pago
	                     $actualizaMora,$montoMora,$actualizaMes,$montoMes,/*datos para actualizar el pago en mora y mensualidad*/
						 $entro,/*variable para saber si tengo que alternar entre la variable monto de venta o monto de proximo pago*/
						 $estaEnMora,/*variable para saber que una vez que los pagos se hayan realizado la nueva fecha generada aun sigue en mora*/
						 $statusMora,/*esta variable me dice si cuando llame mandar la informacion ya estaba en mora*/
	                     $siActualizaMora,$siActualizaMes,$mesParaProximoPago,$siPagueMora,$entreAmensualidad,//datos extras para registrar pagos y mora
	                     $mesesPagoInteres,$montoPagoInteres,$montoPagoMensualidad,$cuantasVecesEntroMora,$montoAbonoInteres,$montoAbonoMensualidad)//conceptos para llenar la tabla pagos
{
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);

	if ($mysqli->connect_errno)
	{
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}
	$respuesta = array();

	$entroEnMora = (int)$siPagueMora;

	///registro pago mora
	if ($entroEnMora > 0)
	{
		$id_Mora = explode(",",$idMora);
		$tama�o = count($id_Mora);

		for($i=0; $i<$tama�o; $i++)
		{
			$query = "UPDATE `Mora` SET $nombreColumna = '$datoStatusMora', `fecha_pago`='$fecha_pago' WHERE `id_mora` = '$id_Mora[$i]'" ;
			$mysqli->query($query);

		}
		$respuesta[0] = $mysqli->affected_rows;
	}
	///fin

	///actualiza Mora
	$si_actualizo_mora = (int)$siActualizaMora;

	if ($si_actualizo_mora == 1)
	{
		$actualiza_Mora = (int) $actualizaMora;
		$query = "UPDATE `Mora` SET `monto_mora`= '$montoMora'  WHERE `id_mora` = ' $actualizaMora'" ;

		$mysqli->query($query);

		$respuesta[1] = $mysqli->affected_rows;

		$queryInsertar = "INSERT INTO `Pago`(`pk_venta`, `monto`, `fecha_pago`, `fecha_corte`, `tipo_pago`, `pago_actual`)
		                  VALUES ('$id_venta', '$montoAbonoInteres', '$fecha_pago', '$mesParaProximoPago', 'Abono Interes', '0')";

		$mysqli->query($queryInsertar);

	}
	///fin

	///registro pago mensualidad y pago de interes mensual
	$mesQueSePago = explode(",",$fecha_corte);
	$tama�oMesQueSePago = count($mesQueSePago);
	$pagueUnaMensualidad = (int)$entreAmensualidad;

	if ($cuantasVecesEntroMora > 0)
	{
		$mesesPagoInteresArray = explode(",",$mesesPagoInteres);
		$montoPagoInteresArray = explode(",",$montoPagoInteres);
		//$pagoActualInteres = $pago_actual;

		for($i = 0; $i < $cuantasVecesEntroMora; $i++)
		{
			$query = "INSERT INTO `Pago`(`pk_venta`, `monto`, `fecha_pago`, `fecha_corte`, `tipo_pago`, `pago_actual`) VALUES ('$id_venta', '$montoPagoInteresArray[$i]', '$fecha_pago', '$mesesPagoInteresArray[$i]', 'Pago Interes', '0')";
			$mysqli->query($query);
			//$pagoActualInteres++;
		}
	}

	if ($pagueUnaMensualidad > 0)
	{
		$pagoActual;
    	$montoMes; /*= $monto;*/
    	$siElMontoEstabaActualizado = (int)$entro;
		$montoPagoMensualidadArray = explode(",",$montoPagoMensualidad);

	    if ($siElMontoEstabaActualizado == 1)
    	{
	 		$montoMes = $montoP;
  		}

		for ($i = 0; $i < $tama�oMesQueSePago ; $i++)
		{
			$query = "INSERT INTO `Pago`(`pk_venta`, `monto`, `fecha_pago`, `fecha_corte`, `tipo_pago`, `pago_actual`) VALUES ('$id_venta', '$montoPagoMensualidadArray[$i]', '$fecha_pago', '$mesQueSePago[$i]', 'Pago Mensualidad', '$pago_actual')";
			$mysqli->query($query);
			$montoMes = $monto;

			$pagoActual = (int)$pago_actual;
			$pagoActual++;
			$pago_actual = (string)$pagoActual;
		}
		$respuesta[2] = $mysqli->affected_rows;
	}
	///fin

	/// registrar proximo pago
	if ($pagueUnaMensualidad > 0)
	{
		$query = "UPDATE `ProximoPago` SET `monto`='$monto',`proximo_pago`='$mesParaProximoPago',`pago_actual`='$pago_actual',`status_mora`='$estaEnMora' WHERE `pk_venta` = '$id_venta'";
		$mysqli->query($query);

		$respuesta[3] = $mysqli->affected_rows;
	}
	///fin

	///update proximo pago
	$si_actualiza_Mes = (int)$siActualizaMes;

	if ($si_actualiza_Mes > 0)
	{
		$query = "UPDATE `ProximoPago` SET `monto`= '$montoMes' WHERE `pk_venta` = '$id_venta'" ;
		$mysqli->query($query);

		$queryInsertar = "INSERT INTO `Pago`(`pk_venta`, `monto`, `fecha_pago`, `fecha_corte`, `tipo_pago`, `pago_actual`)
		                  VALUES ('$id_venta', '$montoAbonoMensualidad', '$fecha_pago', '$mesParaProximoPago', 'Abono Mensualidad', '0')";

		$mysqli->query($queryInsertar);

		$respuesta[4] = $mysqli->affected_rows;

		$mysqli->close();
	}

	//if ($respuesta[0] == 1)
	//{
		return new soapval('return','xsd:string', "Pago generado con EXITO ");
	//}
	//else
	//{
	//	return new soapval('return','xsd:string', "false");
	//}
}

function registraPago($id_venta, $monto, $fecha_pago, $fecha_corte, $tipo_pago, $pago_actual){

	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}

	$query = "INSERT INTO `Pago`(`pk_venta`, `monto`, `fecha_pago`, `fecha_corte`, `tipo_pago`, `pago_actual`) VALUES ('$id_venta', '$monto', '$fecha_pago', '$fecha_corte', '$tipo_pago', '$pago_actual')";
	$result = $mysqli->query($query);
	$mysqli->close();
	return new soapval('return','xsd:string', "pago registrado con EXITO");
}

function getProximoPago($id_venta)
{
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}
	$idVenta = (int) $id_venta;
	$query = "SELECT `monto`,`proximo_pago`, `pago_actual`, `pago_final`, `status_mora` FROM `ProximoPago` WHERE `pk_venta` = '$idVenta'";
	$result = $mysqli->query($query);
	$data = $result->fetch_assoc();
	$monto = $data['monto'];
	$proximo_pago =$data['proximo_pago'];
	$pago_actual = $data['pago_actual'];
	$pago_final = $data['pago_final'];
	$status_mora = $data['status_mora'];
	$mysqli->close();
	return new soapval('return','xsd:string', "$monto,$proximo_pago,$pago_actual,$pago_final,$status_mora");

}

function updateStatusMora($id_venta){
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}
	$idVenta = (int) $id_venta;
	$query = "UPDATE ProximoPago SET status_mora = '1' WHERE pk_venta = '$id_venta'" ;
	$mysqli->query($query);
	$echo = $mysqli->affected_rows;
	if ($echo == 1) {
		return new soapval('return','xsd:string', "Status 1");
	}else{
		return new soapval('return','xsd:string', "No hubo cambio");
	}
}

function getPago($id_venta)
{

/*	$sesion = validaSesion($id_sesion);
	if($sesion != null){
		$id_usuario = $sesion->getId_user();
		$usuario = $sesion->getUsuario();
		if($usuario->getTipo() == 1){
			return new soapval('return','xsd:string', 'IN');
		}else{
			return new soapval('return','xsd:string', 'NO TIENE ACCESO');
		}
	}else{
		return new soapval('return','xsd:string','NO TIENE ACCESO');
	}*/
}

function updateTablaProximoPago($id_venta,$monto)
{
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}
	$idVenta = (int) $id_venta;
	$query = "UPDATE `ProximoPago` SET `monto`= '$monto' WHERE `id_venta` = '$idVenta'" ;
	$mysqli->query($query);
	$echo = $mysqli->affected_rows;
	if ($echo == 1) {
		return new soapval('return','xsd:string', "Status 1");
	}else{
		return new soapval('return','xsd:string', "No hubo cambio");
	}
	/*$sesion = validaSesion($id_sesion);
	if($sesion != null){
		$id_usuario = $sesion->getId_user();
		$usuario = $sesion->getUsuario();
		if($usuario->getTipo() == 1){
			return new soapval('return','xsd:string', 'IN');
		}else{
			return new soapval('return','xsd:string', 'NO TIENE ACCESO');
		}
	}else{
		return new soapval('return','xsd:string','NO TIENE ACCESO');
	}*/
}

function updateProximoPago($id_venta, $mensualidad, $proximo_pago, $pago_actual, $pago_final, $status_mora){

	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}
	$idVenta = (int) $id_venta;
	$respuestaDelete = "";

	$query = "UPDATE `ProximoPago` SET `monto`='$mensualidad',`proximo_pago`='$proximo_pago',`pago_actual`='$pago_actual',`pago_final`='$pago_final',`status_mora`='$status_mora' WHERE `pk_venta` ='$idVenta'";

	$mysqli->query($query);
	$echo = $mysqli->affected_rows;


	if ($status_mora != 1)
	{
		$queryDeleteMora = "DELETE FROM `Mora` WHERE `pk_venta` = '$id_venta'";
		$mysqli->query($queryDeleteMora);
		$echoDelete = $mysqli->affected_rows;

		if ($echoDelete > 0)
		{
			$respuestaDelete = "Adeudo de interes eliminado";
		}
	}
	$mysqli->close();

	if ($echo == 1)
	{
		return new soapval('return','xsd:string', "Proximo pago modificado con EXITO! ".$respuestaDelete);
	}
	else
	{
		return new soapval('return','xsd:string', "No se realiza la consulta");
	}
}

function registrarNuevoProximoPago($id_venta,$monto, $proximo_pago, $pago_actual, $status_mora){

	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);
	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}
	$idVenta = (int) $id_venta;
	$query = "UPDATE `ProximoPago` SET `monto`='$monto',`proximo_pago`='$proximo_pago',`tipo_pago`='$tipo_pago',`pago_actual`='$pago_actual',`status_mora`='$status_mora' WHERE `id_venta` = '$idVenta' " ;
	$mysqli->query($query);
	$echo = $mysqli->affected_rows;

	if ($echo == 1) {
		return new soapval('return','xsd:string', "1");
	}
    else
    {
		return new soapval('return','xsd:string', "no realiza cambios");
	}
}

function insertarDetallePago($idVenta,
					         $pagoActual,
							 $pagoFinal,
	                         $idLote,
	                         $mensualidad,
	                         $deudaTotal,
	                         $restoAdeudo,
							 $comprador,
	                         $fechaProximoPago,
							 $fechaPago,
	                         $cuantasPagoMora,
	                         $cuantasAbonoMora,
	                         $cuantasPagoMes,
	                         $cuantasAbonoMes,
	                         $montoPagoMora,
	                         $montoAbonoMora,
	                         $montoPagoMes,
	                         $montoAbonoMes,
							 $administracion)
{
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);

	if ($mysqli->connect_errno)
	{
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}

	$query = "INSERT INTO `DetallePago`(`idVenta`, `pagoActual`, `pagoFinal`, `idLote`, `mensualidad`, `deudaTotal`, `restoAdeudo`, `comprador`, `fechaProximoPago`, `fechaPago`,`cuantasPagoMora`, `cuantasAbonoMora`, `cuantasPagoMes`, `cuantasAbonoMes`, `montoPagoMora`, `montoAbonoMora`, `montoPagoMes`, `montoAbonoMes`, `administracion`)
	          VALUES ('$idVenta','$pagoActual','$pagoFinal','$idLote','$mensualidad','$deudaTotal','$restoAdeudo', '$comprador', '$fechaProximoPago','$fechaPago','$cuantasPagoMora','$cuantasAbonoMora','$cuantasPagoMes','$cuantasAbonoMes','$montoPagoMora','$montoAbonoMora',' $montoPagoMes',' $montoAbonoMes', '$administracion')";

	$result = $mysqli->query($query);

	$echo = $mysqli->affected_rows;

	$id = $mysqli->insert_id;

	$mysqli->close();

	if($echo == 1)
	{
		return new soapval('return','xsd:string', "Se registro detalle pago".",".$id);
	}
	else
	{
		return new soapval('return','xsd:string', "fallo".","."0");
	}
}

function getDetallePago($condicion)
{
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);

	if ($mysqli->connect_errno){
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}

	$query = "SELECT `comprador`, `montoPagoMora`, `montoAbonoMora`, `montoPagoMes`, `montoAbonoMes`, `idLote`, `fechaProximoPago`, `fechaPago`, `idVenta`, `idDetallePago`, `pagoActual`, `restoAdeudo`, `cuantasPagoMora`, `cuantasAbonoMora`, `cuantasPagoMes`, `cuantasAbonoMes` FROM `DetallePago` INNER JOIN `RegistroActividad` AS `r` ON  `idDetallePago` = r.`idActividad` WHERE $condicion";
	$datos = array();
	//return new soapval('return','xsd:string', $query);
	if($result = $mysqli->query($query))
	{
		//return new soapval('return','xsd:string', "Si entre y esta es la cantidad de resultados que encontre ".$result->num_rows);
		if ($mysqli->affected_rows > 0)
		{
			while ($data = $result->fetch_assoc())
			{
				$comprador = $data['comprador'];
				$montoPagoMora =  $data['montoPagoMora'];
				$montoAbonoMora =  $data['montoAbonoMora'];
				$montoPagoMes =  $data['montoPagoMes'];
				$montoAbonoMes =  $data['montoAbonoMes'];
				$idLote = $data['idLote'];
				$fechaProximoPago = $data['fechaProximoPago'];
				$fechaPago = $data['fechaPago'];
				$idVenta = $data['idVenta'];
				$idDetallePago = $data['idDetallePago'];
				$pagoActual =$data['pagoActual'];
				$restoAdeudo = $data['restoAdeudo'];
				$cuantasPagoMora = $data['cuantasPagoMora'];
				$cuantasAbonoMora = $data['cuantasAbonoMora'];
				$cuantasPagoMes = $data['cuantasPagoMes'];
				$cuantasAbonoMes = $data['cuantasAbonoMes'];

				////Query id manzana
				$queryIdManzana = "SELECT `pk_manzana` FROM `Lote` WHERE `id_lote` = '$idLote' " ;

				$resultIdManzana = $mysqli->query($queryIdManzana);
				$dataIdManzana = $resultIdManzana->fetch_assoc();

				$idManzana = $dataIdManzana['pk_manzana'];
				////fin

				////Query numero de manzana
				$queryNumeroManzana = "SELECT n_manzana FROM Manzana WHERE id_manzana = '$idManzana' " ;

				$resultNumeroManzana = $mysqli->query($queryNumeroManzana);
				$dataNumeroManzana = $resultNumeroManzana->fetch_assoc();

				$numero_manzana = $dataNumeroManzana['n_manzana'];
				////fin

				////Query id predio
				$queryIdPredio = "SELECT `pk_predio` FROM `Lote` WHERE `id_lote` = '$idLote' " ;

				$resultIdPredio = $mysqli->query($queryIdPredio);
				$dataIdPredio = $resultIdPredio->fetch_assoc();

				$id_predio = $dataIdPredio['pk_predio'];
				////fin

				////Query nombre predio
				$queryNombrePredio = "SELECT nombre_predio FROM `Predio` WHERE id_predio = '$id_predio'" ;

				$resultNombrePredio = $mysqli->query($queryNombrePredio);
				$dataNombrePredio = $resultNombrePredio->fetch_assoc();

				$nombre_predio = $dataNombrePredio['nombre_predio'];
				////fin

				////Query numero lote
				$queryNumeroLote = "SELECT n_lote FROM Lote WHERE id_lote = '$idLote'" ;

				$resultNumeroLote = $mysqli->query($queryNumeroLote);
				$dataNumeroLote = $resultNumeroLote->fetch_assoc();

				$numero_lote = $dataNumeroLote['n_lote'];
				////fin

				$datos[] = $comprador.",".$montoPagoMora.",".$montoAbonoMora.",".$montoPagoMes.",".$montoAbonoMes.",".$numero_lote.",".$numero_manzana.",".$fechaProximoPago.",".$fechaPago.",".$nombre_predio.",".$idVenta.",".$idDetallePago.",".$pagoActual.",".$restoAdeudo.",".$cuantasPagoMora.",".$cuantasAbonoMora.",".$cuantasPagoMes.",".$cuantasAbonoMes;
			};

			$DatosPredio = implode("|",$datos);

			$mysqli->close();

			return new soapval('return','xsd:string', $DatosPredio);
		}
		else
		{
			$mysqli->close();

			return new soapval('return','xsd:string', "0");

		}
	}
	else
	{
		$mysqli->close();

		return new soapval('return','xsd:string', "Error al generar la consulta");
	}

}

function arreglarDetallePago()
{
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);

	if ($mysqli->connect_errno)
	{
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}

	$query = "SELECT `idDetallePago`, SUBSTRING_INDEX(  `montoPagoMora` ,  '$', -1 ) AS `MontoMora` FROM  `DetallePago` ORDER BY  `idDetallePago` ASC";

	$idDetalle = array();
	$montoMora = array();
	$result = $mysqli->query($query);

	while ( $data = $result->fetch_assoc())
	{
		$idDetalle[] = $data['idDetallePago'];
		$montoMora[] = $data ['MontoMora'];
	};

	/*$id = implode(",",$idDetalle);
	$monto = implode(",",$montoMora);
	$mysqli->close();*/

	$contador = count($idDetalle);
	$contador2 = count($montoMora);
	$j = 0;
	$result2;

	//return new soapval('return','xsd:string', $contador." ".$contador2." ".$id." ".$monto/*$result2." ".$id." ".$monto*/);

	for ($i = 0; $i < $contador; $i++)
	{
		$query2 = "UPDATE `DetallePago` SET `montoPagoMora`= $montoMora[$i] WHERE `idDetallePago` = $idDetalle[$i]";
		$result2 = $mysqli->query($query2);
		$j++;
	}

	/*$id = implode(",",$idDetalle);
	$monto = implode(",",$montoMora);
	$mysqli->close();*/

	return new soapval('return','xsd:string', $result2." ".$j." ".$contador);
}

function consultaDetallePago($query, $tipo)
{
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);

	if ($mysqli->connect_errno)
	{
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}

	$result = $mysqli->query($query);
	$filasAfectadas = $mysqli->affected_rows;
	$respuesta;

	switch ($tipo)
	{
		case 1:
			if($result)
			{
				if($filasAfectadas > 0)
				{
					$respuesta = "Registro eliminado con EXITO!!";
				}
				else
				{
					$respuesta = "La consulta se realizo pero NO ELIMINO ningun registro";
				}
			}
			else
			{
				$respuesta = "No se realizo la consulta";
			}

			return new soapval('return','xsd:string', $respuesta);
			;
			break;

		case 2:
			if($result)
			{
				if($filasAfectadas > 0)
				{
					$respuesta = "Registro modificado con EXITO!!";
				}
				else
				{
					$respuesta = "La consulta se realizo pero NO MODIFICO ningun registro";
				}
			}
			else
			{
				$respuesta = "No se realizo la consulta";
			}

			return new soapval('return','xsd:string', $respuesta." ".$filasAfectadas);
			;
			break;

		default:
			;
	} // switch
}

function getHistorialPagos($predio, $manzana)
{
	$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAM);

	if ($mysqli->connect_errno)
	{
		printf("conexion fallida %s/n",$mysqli->connect_errno);
		exit();
	}

	$query = "SELECT id_predio FROM `Predio` WHERE nombre_predio = '$predio'" ;
	$result = $mysqli->query($query);
	$data = $result->fetch_assoc();
	$idPredio = $data['id_predio'];

	$query = "SELECT id_manzana FROM Manzana WHERE n_manzana = '$manzana' AND pk_predio = '$idPredio' " ;
	$result = $mysqli->query($query);
	$data = $result->fetch_assoc();
	$idManzana = $data['id_manzana'];


	//return new soapval('return','xsd:string', $idPredio." ".$idManzana);



	$query = "SELECT `pk_comprador`, `pk_lote` FROM `CompraVenta`
	           WHERE `pk_lote`IN ( SELECT `id_lote` FROM `Lote` WHERE `pk_predio` = '$idPredio' AND `pk_manzana` = '$idManzana')
	           ORDER BY `pk_lote` ASC";

	$idComprador = array();
	$idLote = array();
	$datos = array();
	$stringDatos;
	$stringids;
	$nombreComprador;
	$numeroLote;
	//$telefono2;

	$result = $mysqli->query($query);

	if ($result)
	{
		while ($data = $result->fetch_assoc())
		{
			$idComprador[] = $data['pk_comprador'];
			$idLote[] = $data['pk_lote'];
		};
		//$stringDatos = implode(",",$idComprador);
		//$stringids = implode(",",$idLote);

		//return new soapval('return','xsd:string', $stringDatos." ".$stringids);

		$tama�oIdComprador = count($idComprador);

		if ($result->num_rows > 0)
		{
			$result->close();

			for($i = 0; $i < $tama�oIdComprador; $i++)
			{
				$query = "SELECT `nombre`, `n_lote` FROM `Comprador`, `Lote` WHERE `id_comprador` = '$idComprador[$i]' AND `id_lote`= '$idLote[$i]'";

				$result = $mysqli->query($query);

				if ($result->num_rows > 0)
				{
					$data = $result->fetch_assoc();

					$nombreComprador = $data['nombre'];
					$numeroLote = $data['n_lote'];

					$datos[] = $nombreComprador.",".$numeroLote;
				}
				//$clientesMorosos = implode("|",$idVenta);
				//return new soapval('return','xsd:string', $clientesMorosos." ".$nombreComprador." ".$telefono1." ".$datos[0]);
			}
			//$result->close();
			//$mysqli->close();
			$datosCliente = implode("|",$datos);

			//"SELECT `comprador`,`montoPagoMora`, `montoAbonoMora`, `montoPagoMes`, `montoAbonoMes`, `idLote`,`fechaProximoPago`, `fechaPago`, `idVenta`, `idDetallePago`, `pagoActual`, `restoAdeudo`, `cuantasPagoMora`, `cuantasAbonoMora`, `cuantasPagoMes`, `cuantasAbonoMes` FROM `DetallePago` WHERE $condicion


			//$datosDetallePago = array();
			$datosDetallePago = "";
			$tama�oIdLote = count($idLote);

			for($i=0; $i < $tama�oIdLote; $i++)
			{
				$query = "SELECT `montoPagoMora`, `montoAbonoMora`, `montoPagoMes`, `montoAbonoMes`,`fechaProximoPago`, `fechaPago`, `pagoActual` FROM `DetallePago` WHERE `idLote` = '$idLote[$i]' ORDER BY `fechaPago` DESC";

				if($result = $mysqli->query($query))
				{
					if ($mysqli->affected_rows > 0)
					{
						while ($data = $result->fetch_assoc())
						{
							//$comprador = $data['comprador'];
							$montoPagoMora =  $data['montoPagoMora'];
							$montoAbonoMora =  $data['montoAbonoMora'];
							$montoPagoMes =  $data['montoPagoMes'];
							$montoAbonoMes =  $data['montoAbonoMes'];
							//$idLote = $data['idLote'];
							$fechaProximoPago = $data['fechaProximoPago'];
							$fechaPago = $data['fechaPago'];
							//$idVenta = $data['idVenta'];
							//$idDetallePago = $data['idDetallePago'];
							$pagoActual =$data['pagoActual'];
							//$restoAdeudo = $data['restoAdeudo'];
							//$cuantasPagoMora = $data['cuantasPagoMora'];
							//$cuantasAbonoMora = $data['cuantasAbonoMora'];
							//$cuantasPagoMes = $data['cuantasPagoMes'];
							//$cuantasAbonoMes = $data['cuantasAbonoMes'];

							$datosDetallePago .= $montoPagoMora.",".$montoAbonoMora.",".$montoPagoMes.",".$montoAbonoMes.",".$fechaProximoPago.",".$fechaPago.",".$pagoActual."?";
						};
					}
				}
				$datosDetallePago .= "|";
			}

			//$DatosDetalle = implode("|",$datosDetallePago);

			return new soapval('return','xsd:string', $datosCliente."@".$datosDetallePago);
		}
		else
		{
			return new soapval('return','xsd:string', "0");
		}

		/*$filasAfectadas = $mysqli->affected_rows;

		   if ($filasAfectadas > 0)
		   {
		   return new soapval('return','xsd:string', $clientesMorosos);
		   }
		   else
		   {
		   return new soapval('return','xsd:string', "La consulta no genero ningun resultado");
		   }*/
	}
	else
	{
		return new soapval('return','xsd:string', "Error al ejecutar la consulta");
	}
}
?>